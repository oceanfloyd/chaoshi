
var app = getApp();
var util = require('../../../utils/util.js');
var WxParse = require('../../../components/wxParse/wxParse.js');
Page({
  data: {
    articleId: '',
    articleInfo: {},
    likeLogCount: '',
    likeLogItems: [],
    is_liked: '',
    commentWidth: '',
    commentList: [],
    commentTop: '',
    couponList: '',
    payPrice: '',
    radioCheckVal: 0,
    child_comment: [],
    article_style: '',
    showMask: false,
    showCustom: false,
    pricePay: false,
    getCommentData: {
      page: 1,
      loading: false,
      nomore: false
    },
    commentCount: '',
    imgData: {},
    showAddArticleBtn: true,
    theme_color: '#00b6f8',
    thumbUpBackgroundColor: '#3091f2',
    thumbUpColor: '#fff',
    address: '',
    scrollInto: ''
  },
  onLoad: function (options) {
    let that = this,
      articleId = options.detail,
      thumbUpBackgroundColor = options.dataLiked == '1' ? '#f2f2f2' : '#3091f2',
      thumbUpColor = options.dataLiked == '1' ? '#3091f2' : '#fff',
      likeLogCount = options.dataLiked == '1' ? '' : '赞';

    this.setData({
      articleId: articleId,
      thumbUpBackgroundColor: thumbUpBackgroundColor,
      thumbUpColor: thumbUpColor,
      likeLogCount: likeLogCount,
    });
    this.getArticleInfo();
    this.countRead();
    // this.getLikeLog();
    this.getComment();

    app.globalData.communityDetailRefresh = false;
  },
  onShow: function () {
    if (app.globalData.communityDetailRefresh) {
      this.setData({
        getCommentData: {
          page: 1,
          loading: false,
          nomore: false
        },
        commentList: []
      });
      app.globalData.communityDetailRefresh = false;
      this.getComment();
    }
  },
  getArticleInfo: function () {
    var that = this;
    var _this = this;
    app.sendRequest({
      url: '/index.php?r=AppNews/GetArticleByPage',
      data: {
        article_id: that.data.articleId,
        page: 1,
        is_detail: 1,
        page_size: 100,
      },
      method: 'post',
      success: function (res) {
        if (res.status == 0) {
          let info = res.data[0],
            description = info.content,
            couponList = info.recommend;

          // info.title = unescape(info.title.replace(/\\u/g, "%u"));
          // info.content_text = info.content.replace(/\n|\\n/g, '\n');

          that.setData({
            articleInfo: info,
            couponList: couponList
          });
          WxParse.wxParse('wxParseDescription', 'html', description, _this, 10);

          // that.getThemeColor(info.section_id);

          // that.getLocByLatAndLng(info.latitude, info.longitude, function (data) {
          //   that.setData({ address: data.address });
          // });
        }
      }
    });
  },
  countRead: function () {
    var that = this;
    app.sendRequest({
      url: '/index.php?r=AppNews/CountReadCount',
      data: {
        article_id: that.data.articleId,
      },
      method: 'post',
    });
  },
  // getLikeLog: function () {
  //   var that = this;
  //   app.sendRequest({
  //     url: '/index.php?r=AppNews/GetLikeLogByPage',
  //     data: {
  //       page: 1,
  //       obj_type: 1,
  //       obj_id: that.data.articleId,
  //       page_size: 10
  //     },
  //     method: 'post',
  //     success: function (res) {
  //       if (res.status == 0) {
  //         let info = res.data;
  //         res.count = that.data.is_liked == '1' ? res.count : '赞'
  //         that.setData({
  //           likeLogCount: res.count,
  //           likeLogItems: info,
  //         });
  //       }
  //     }
  //   });
  // },
  rewardCancel: function () {
    var showMask = false;
    this.setData({
      showMask: showMask
    })
  },
  radioCheckedChange: function (e) {
    this.setData({
      radioCheckVal: e.detail.value
    })
  },
  customPriceBlur: function (e) {
    this.setData({
      payPrice: e.detail.value
    })
  },
  payPrice: function (event) {
    this.setData({
      payPrice: event.currentTarget.dataset.price,
    })
  },
  confirmationOfpayment: function (e) {
    var list = this.data.goodsList,
      that = this,
      selected_benefit = this.data.selectDiscountInfo,
      hasWritedAdditionalInfo = false;
	  if (this.data.payPrice < 1 || this.data.payPrice > 200) {
      app.showModal({
        content: '请输入1-200的数值',
      })
      return false;
    }
    app.sendRequest({
      url: '/index.php?r=AppNews/AddRewardOrder',
      method: 'post',
      data: {
        article_id: that.data.articleId,
        price: that.data.payPrice,
      },
      success: function (res) {
        that.payOrder(res.data);
      },
      fail: function () {
        that.requesting = false;
      },
      successStatusAbnormal: function () {
        that.requesting = false;
      }
    });
  },
  payOrder: function (orderId) {
    var that = this;

    function paySuccess() {
      app.turnToPage('/informationManagement/pages/newsDetail/newsDetail?detail=' + that.data.articleId,1);
      wx.showToast({
        title: '打赏成功',
        icon: 'success',
      })
    }

    function payFail() {
      var showCustom = false;
      that.setData({
        showCustom: showCustom
      })
    }
    app.sendRequest({
      url: '/index.php?r=AppNews/GetWxWebappPaymentCode',
      data: {
        order_id: orderId
      },
      success: function (res) {
        var param = res.data;

        param.orderId = orderId;
        param.success = paySuccess;
        param.fail = payFail;
        that.wxPay(param);
      },
      fail: function () {
        payFail();
      },
      successStatusAbnormal: function () {
        payFail();
      }
    })
  },
  wxPay: function (param) {
    var that = this;
    wx.requestPayment({
      'timeStamp': param.timeStamp,
      'nonceStr': param.nonceStr,
      'package': param.package,
      'signType': 'MD5',
      'paySign': param.paySign,
      success: function (res) {
        app.wxPaySuccess(param);
        param.success();
      },
      fail: function (res) {
        if (res.errMsg === 'requestPayment:fail cancel') {
          param.fail();
          return;
        }
        app.showModal({
          content: '支付失败',
          complete: param.fail
        })
        app.wxPayFail(param, res.errMsg);
      }
    })
  },
  rewardPlay: function () {
    var showMask = true;
    this.setData({
      showMask: showMask
    })
  },
  customByReward: function () {
    var showMask = false,
      showCustom = true;
    this.setData({
      showMask: showMask,
      showCustom: showCustom
    })
  },
  cancelPay: function () {
    var showCustom = false;
    this.setData({
      showCustom: showCustom
    })
  },
  getComment: function () {
    var that = this,
      sdata = that.data.getCommentData;

    if (sdata.loading || sdata.nomore) {
      return;
    }
    sdata.loading = true;
    app.sendRequest({
      url: '/index.php?r=AppNews/GetCommentByPage',
      data: {
        page: sdata.page,
        obj_id: that.data.articleId,
        page_size: 10,
        // article_style : 0          
        article_style: that.data.article_style
      },
      method: 'post',
      success: function (res) {
        if (res.status == 0) {
          let info = res.data,
            oldData = that.data.commentList,
            newData = info;

          for (var i = 0; i < newData.length; i++) {
            let item = newData[i],
              likecount = item.like_count;

            item.like_count_text = likecount <= 0 ? '赞' : (likecount > 10000 ? (Math.floor(likecount / 10000) + '万') : likecount);
            item.likeAnimateShow = true;
          }

          newData = oldData.concat(newData);

          that.setData({
            commentList: newData,
            commentCount: res.count,
            'getCommentData.page': sdata.page + 1
          });
        }
        that.setData({
          'getCommentData.loading': false,
          'getCommentData.nomore': res.is_more == 0 ? true : false
        });
      },
      fail: function (res) {
        that.setData({
          'getCommentData.loading': false
        });
      }
    });
  },
  scrollTolower: function (event) {
    this.getComment();
  },
  oldscrolltop: 0,
  scrollEvent: function (event) {
    let scrolltop = event.detail.scrollTop,
      oldscrolltop = this.oldscrolltop;

    if (scrolltop - oldscrolltop > 60) {
      this.oldscrolltop = scrolltop;
      this.setData({
        showAddArticleBtn: false
      });
    } else if (oldscrolltop - scrolltop > 60) {
      this.oldscrolltop = scrolltop;
      this.setData({
        showAddArticleBtn: true
      });
    }
  },
  imgLoad: function (event, ) {
    let owidth = event.detail.width,
      oheight = event.detail.height,
      oscale = owidth / oheight,
      cwidth = 290,
      cheight = 120,
      ewidth, eheight,
      newData = {};

    if (oscale > cwidth / cheight) {
      ewidth = cwidth;
      eheight = cwidth / oscale;
    } else {
      ewidth = cheight * oscale;
      eheight = cheight;
    }

    newData['imgData'] = {
      imgWidth: ewidth * 2.34,
      imgHeight: eheight * 2.34
    }
    this.setData(newData);
  },
  // getThemeColor: function (section_id) {
  //   var that = this;
  //   app.sendRequest({
  //     url: '/index.php?r=AppNews/GetSectionByPage',
  //     data: {
  //       page: 1,
  //       section_id: section_id,
  //     },
  //     method: 'post',
  //     success: function (res) {
  //       if (res.status == 0) {
  //         let info = res.data[0];

  //         that.setData({
  //           theme_color: info.theme_color,
  //         });
  //       }
  //     }
  //   });
  // },
  turnReply: function (event) {
    let commentId = event.currentTarget.dataset.id,
      sectionId = event.currentTarget.dataset.sectionid,
      parentCommentId = event.currentTarget.dataset.id,
      articleId = this.data.articleId,
      replyto = event.currentTarget.dataset.replyto;

    this.setData({
      showReplyBox: true,
      replyPlaceholder: '@' + replyto,
      replyParam: {
        obj_id: articleId, // 话题id
      }
    });

  },
  turnComment: function (event) {
    let sectionId = event.currentTarget.dataset.sectionid,
      commentId = event.currentTarget.dataset.id,
      parentCommentId = event.currentTarget.dataset.parentid,
      articleId = this.data.articleId,
      replyto = event.currentTarget.dataset.replyto;

    this.setData({
      showReplyBox: true,
      replyPlaceholder: "@" + replyto,
      replyParam: {
        obj_id: articleId, // 话题id
      }
    });
  },
  // turnToPublish: function (event) {
  //   app.turnToPage('/informationManagement/pages/communityPublish/communityPublish?detail=' + this.data.articleInfo.section_id);
  // },
  articleLike: function (event) {
    var that = this,
      liked = event.currentTarget.dataset.liked;
    if (liked == '1') {
      app.showToast({ title: '暂不支持取消' });
      return false;
    }
    app.sendRequest({
      url: '/index.php?r=AppNews/PerformLike',
      data: {
        obj_type: 1,  // obj_type 1-话题 2-评论
        obj_id: that.data.articleId     // obj_id 话题或评论的id
      },
      method: 'post',
      success: function (res) {
        if (res.status == 0) {

          // that.getLikeLog();

          if (liked == 1) {
            that.setData({
              'articleInfo.is_liked': 0,
              thumbUpBackgroundColor: '#3091f2',
              thumbUpColor: '#fff',
              is_liked: '0',
            });
            app.showToast({ title: '点赞取消成功' });
          } else {
            that.setData({
              'articleInfo.is_liked': 1,
              'articleInfo.like_count': that.data.articleInfo.like_count * 1 + 1,
              thumbUpBackgroundColor: '#f2f2f2',
              thumbUpColor: '#3091f2',
              is_liked: '1'
            });
            app.showToast({ title: '点赞成功' });
          }

          app.globalData.communityPageRefresh = true;
        }
      }
    });
  },
  commentLike: function (event) {
    var that = this,
      liked = event.currentTarget.dataset.liked,
      id = event.currentTarget.dataset.id,
      index = +event.currentTarget.dataset.index;

    app.sendRequest({
      url: '/index.php?r=AppNews/PerformLike',
      data: {
        obj_type: 2,  // obj_type 1-话题 2-评论
        obj_id: id     // obj_id 话题或评论的id
      },
      method: 'post',
      success: function (res) {
        if (res.status == 0) {

          if (liked == 1) {
            var newData = {},
              likecount = +that.data.commentList[index].like_count - 1;
            newData['commentList[' + index + '].is_liked'] = 0;
            newData['commentList[' + index + '].like_count'] = likecount;
            newData['commentList[' + index + '].like_count_text'] = likecount <= 0 ? '赞' : (likecount > 10000 ? (Math.floor(likecount / 10000) + '万') : likecount);
            newData['commentList[' + index + '].likeAnimateShow'] = false;

            that.setData(newData);
            app.showToast({ title: '点赞取消成功' });
          } else {
            var newData = {},
              likecount = +that.data.commentList[index].like_count + 1;
            newData['commentList[' + index + '].is_liked'] = 1;
            newData['commentList[' + index + '].like_count'] = likecount;
            newData['commentList[' + index + '].like_count_text'] = likecount <= 0 ? '赞' : (likecount > 10000 ? (Math.floor(likecount / 10000) + '万') : likecount);
            newData['commentList[' + index + '].likeAnimateShow'] = false;

            that.setData(newData);
            app.showToast({ title: '点赞成功' });
          }
          setTimeout(function () {
            let newData = {};
            newData['commentList[' + index + '].likeAnimateShow'] = true;
            that.setData(newData);
          }, 480);
        }
      }
    });
  },
  previewImage: function (event) {
    let that = this,
      curImg = event.currentTarget.dataset.src;
    app.previewImage({
      current: curImg,
      urls: that.data.articleInfo.content.imgs
    });
  },

  historyBack: function () {
    app.turnBack();
  },
  onShareAppMessage: function (res) {
    let shareTitle = this.data.articleInfo.title;
    let sharePath = util.getCurrentPageUrlWithArgs();
    console.log(shareTitle, sharePath)
    return {
      title: shareTitle,
      path: sharePath,
      success: function (res) {
        app.showToast({ title: '转发成功' });
      },
      fail: function (res) { }
    }
  },
  turnToReport: function () {
    app.turnToPage('/informationManagement/pages/communityReport/communityReport?detail=' + this.data.articleId);
  },
  gotoCouponDetail: function (event) {
    let id = event.currentTarget.dataset.couponId;
    app.turnToPage('/eCommerce/pages/couponDetail/couponDetail?couponStatus=recieve&detail=' + id);
  },
  turnToArticle: function (event) {
    let id = event.currentTarget.dataset.id;
    app.turnToPage('/informationManagement/pages/newsDetail/newsDetail?detail=' + id);
  },
  turnToGoodsDetail: function (event) {
    let id = event.currentTarget.dataset.id,
      style = event.currentTarget.dataset.style;
    if (style == 3) {
      app.turnToPage('/orderMeal/pages/toStoreDetail/toStoreDetail?detail=' + id);
    } else {
      app.turnToPage('/eCommerce/pages/goodsDetail/goodsDetail?detail=' + id);
    }
  },
  receiveCoupon: function (event) {
    let _this = this;
    let couponId = event.currentTarget.dataset.couponId;
    app.sendRequest({
      url: '/index.php?r=AppShop/recvCoupon',
      data: {
        coupon_id: event.currentTarget.dataset.couponId
      },
      hideLoading: true,
      success: function (res) {
        _this.setData({
          receiveSuccess: 1,
          receiveCount: res.data.recv_count,
          receiveLimitNum: res.data.limit_num
        });
        setTimeout(function () {
          _this.hideToast();
        }, 3000);
        if (res.data.is_already_recv == 1) {
          let couponList = _this.data.couponList;
          for (var i = 0; i < couponList.length; i++) {
            if (couponList[i]['id'] == couponId) {
              let newData = {};
              newData['couponList[' + i + '].recv_status'] = 0;
              _this.setData(newData);
            }
          }
        }
      }
    })
  },
  cancelReply: function () {
    this.setData({ showReplyBox: false });
  },
  submitReply: function () {
    let that = this,
      replyText = that.data.replyParam.content;

    if (/^\s*$/.test(replyText) || !replyText) {
      app.showModal({ content: '请填写回复内容' });
      return;
    }

    app.sendRequest({
      url: '/index.php?r=AppNews/AddComment',
      data: that.data.replyParam,
      method: 'post',
      success: function (res) {
        if (res.status == 0) {
          app.showToast({
            title: '回复成功',
            success: function () {
              that.setData({ showReplyBox: false });
              delete that.data.replyParam;
            }
          });
          app.globalData.communityDetailRefresh = true;
          that.onShow();
        }
      }
    });
  },
  replyInput: function (event) {
    this.setData({
      'replyParam.content': event.detail.value
    });
  },
  scrollTo: function (e) {
    let id = e.currentTarget.dataset.id;
    this.setData({
      scrollInto: id
    })
  },

  // getLocByLatAndLng: function (lat, lng, cb) {
  //   app.sendRequest({
  //     url: '/index.php?r=Map/GetAreaInfoByLatAndLng',
  //     data: {
  //       latitude: lat,
  //       longitude: lng
  //     },
  //     method: 'post',
  //     success: function (data) {
  //       if (data.status == 0 && typeof cb == 'function') {
  //         cb(data.data);
  //       }
  //     }
  //   })
  // }
})
